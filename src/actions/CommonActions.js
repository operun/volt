import axios from 'axios';
import {
  SET_CURRENT_ITEM,
  GET_REQUEST_,
  GET_SUCCESS_,
  EDIT_ITEM,
  CLOSE_ITEM,
  SAVE_ITEM_REQUEST,
  SAVE_ITEM_SUCCESS,
  SAVE_SUCCESS_,
  UPDATE_ITEM_SUCCESS,
  UPDATE_SUCCESS_,
  DELETE_ITEM,
  DELETE_ITEM_SUCCESS,
  DELETE_SUCCESS_,
  ROUTING
} from '../constants/CommonConstants'

export function getItems(type) {
  return (dispatch) => {
    dispatch({
      type: GET_REQUEST_ +type.toUpperCase()
    })

    axios.get('/api/'+type)
      .then(function (response) {
        console.log(response);
        dispatch({
          type: GET_SUCCESS_ +type.toUpperCase(),
          payload: response.data
        })
      })
      .catch(function (error) {
        console.log(error);
      });
  }
}

export function setCurrentItem(item) {
  return (dispatch) => {

    dispatch({
      type: SET_CURRENT_ITEM,
      payload: item
    })
  }
}

export function editItem(item) {
  return (dispatch) => {

    dispatch({
      type: EDIT_ITEM,
      payload: item
    })
  }
}

export function closeItem() {
  return (dispatch) => {

    dispatch({
      type: CLOSE_ITEM
    })
  }
}

export function saveItem(type, item) {
  return (dispatch) => {

    dispatch({
      type: SAVE_ITEM_REQUEST
    })

    if (item.id) {
      axios.put('/api/'+type+'/' + item.id, item)
        .then(function (response) {
          console.log(response);
          dispatch({
            type: UPDATE_ITEM_SUCCESS
          })
          dispatch({
            type: UPDATE_SUCCESS_ +type.toUpperCase(),
            payload: item
          })
        })
        .catch(function (error) {
          console.log(error);
        });
    } else {
      axios.post('/api/'+type, item)
        .then(function (response) {
          console.log(response);
          item.id = response.data.id
          dispatch({
            type: SAVE_ITEM_SUCCESS
          })
          dispatch({
            type: SAVE_SUCCESS_ +type.toUpperCase(),
            payload: {
              currentItem: item
            }
          })
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  }
}

export function deleteItem(item) {
  return (dispatch) => {

    dispatch({
      type: DELETE_ITEM,
      payload: item
    })
  }
}

export function deleteConfirmItem(type, item) {
  return (dispatch) => {

    axios.delete('/api/'+type+'/' + item.id)
      .then(function (response) {
        console.log(response);
        dispatch({
          type: DELETE_ITEM_SUCCESS
        })
        dispatch({
          type: DELETE_SUCCESS_+type.toUpperCase(),
          payload: item
        })
      })
      .catch(function (error) {
        console.log(error);
      });
  }
}

export function redirectUrl(url) {
  return (dispatch) => {

    dispatch({
      type: ROUTING,
      payload: {
        method: 'replace',
        nextUrl: url
      }
    })
  }
}