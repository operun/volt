import axios from 'axios';
import {
  SET_CURRENT_ITEM,
  ROUTING
} from '../constants/CommonConstants'
import {
  SAVE_SUCCESS_INVOICES
} from '../constants/InvoicesConstants'


export function createInvoice() {
  return (dispatch) => {

    axios.post('/api/invoices')
      .then(function (response) {
        let item = response.data

        // dispatch({
        //   type: SET_CURRENT_ITEM,
        //   payload: item
        // })

        dispatch({
          type: SAVE_SUCCESS_INVOICES,
          payload: {currentItem: item}
        })

        dispatch({
          type: ROUTING,
          payload: {
            method: 'replace',
            nextUrl: '/invoices/'+item.id+'/edit'
          }
        })

      })
      .catch(function (error) {
        console.log(error);
      });
  }
}

export function saveInvoice(item) {
  return (dispatch) => {

    axios.put('/api/invoices/'+item.id, item)
      .then(function (response) {
        let item = response.data

        // dispatch({
        //   type: SET_CURRENT_ITEM,
        //   payload: item
        // })

        dispatch({
          type: SAVE_SUCCESS_INVOICES,
          payload: {currentItem: item}
        })

        dispatch({
          type: ROUTING,
          payload: {
            method: 'replace',
            nextUrl: '/invoices/'
          }
        })

      })
      .catch(function (error) {
        console.log(error);
      });
  }
}
