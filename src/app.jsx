import React from 'react'
import {render} from 'react-dom'
import {Provider} from 'react-redux'
import configureStore from './store/configureStore'
import {Router, browserHistory} from 'react-router'
import {routes} from './routes'

// import 'react-select/dist/react-select.css'

const store = configureStore()

render(
  <Provider store={store}>
    <div style={{height: '100%'}}>
      <Router history={browserHistory} routes={routes}/>
    </div>
  </Provider>,
  document.getElementById('app-root')
)