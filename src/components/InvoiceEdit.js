import React, {PropTypes} from 'react'
import {
  Button,
  Form,
  FormGroup,
  Col,
  ControlLabel,
  FormControl,
  Table
} from 'react-bootstrap'
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import TableContainer from '../components/TableContainer'
import axios from 'axios';

export default class InvoiceEdit extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      id: this.props.params.invoiceId,
      customer_id: null,
      discount: 0,
      total: 0,
      customersValue: '',
      customersOptions: this.props.customers.allItems.map((item)=>{
        return {value: item.id, label: item.name}
      }),
      productsValue: null,
      productsOptions: null,
      invoiceItems: [],
      unsaved: true
    }
  }

  componentDidMount(){
    this.props.router.setRouteLeaveHook(this.props.route, () => {
      if (this.state.unsaved)
        this.onSave()
        // return 'You have unsaved information, are you sure you want to leave this page?'
    })

    let me = this
    let invoiceId = this.state.id

    axios.get('/api/invoices/'+invoiceId)
      .then(function (response) {
        if(response.data == null){
          me.props.commonActions.redirectUrl('/invoices')
        }

        let invoice = response.data
        let customersValue = {}
        me.props.customers.allItems.forEach((customer)=>{
          if(customer.id == invoice.customer_id){
            customersValue.value = customer.id
            customersValue.label = customer.name
            return customersValue
          }
        })
        me.setState({
          customersValue: customersValue,
          discount: invoice.discount || 0,
          total: invoice.total || 0,
        })
      })
      .catch(function (error) {
        console.log(error);
      });

    axios.get('/api/invoices/'+invoiceId+'/items')
      .then(function (response) {
        if(response.data != null){

          let invoiceItems = []
          let products = []
          let allProducts = me.props.products.allItems

          allProducts.forEach((product)=>{
            let isDisable = false
            response.data.forEach((item)=>{
              if(product.id == item.product_id){
                item.label = product.name
                item.price = product.price
                invoiceItems.push(item)

                isDisable = true
              }
            })
            products.push({value: product.id, label: product.name, price: product.price, disabled: isDisable})
          })

          me.setState({
            invoiceItems: invoiceItems,
            productsOptions: products
          })
        }

      })
      .catch(function (error) {
        console.log(error);
      });
  }

  onAdd(){
    console.log("onAdd: ")
    let me = this
    let productsValue = this.state.productsValue
    if(productsValue == null){
      return
    }

    let productItem = {}
    productItem.invoice_id = this.state.id
    productItem.product_id = this.state.productsValue.value
    productItem.price = this.state.productsValue.price
    productItem.quantity = 1

    productItem.label = this.state.productsValue.label

    axios.post('/api/invoices/'+this.state.id+'/items', productItem)
      .then(function (response) {
        productItem.id = response.data.id

        let invoiceItems = me.state.invoiceItems
        invoiceItems.push(productItem)

        let total = invoiceItems.reduce((sum, item)=>{
          return sum + (item.price * item.quantity)
        }, 0)

        me.setState({
          total: me.priceWithDiscount(total, me.state.discount),
          productsValue: '',
          productsOptions: me.toggleOptionSelectProduct(productsValue.value),
          invoiceItems: invoiceItems
        })

      })
      .catch(function (error) {
        console.log(error);
      });
  }

  onDelete(product_id, index, e){
    console.log("onDelete: ")
    e.preventDefault()
    let me = this

    let invoiceItems = this.state.invoiceItems
    let invoicePproductId = invoiceItems[index].id

    axios.delete('/api/invoices/'+this.state.id+'/items/'+invoicePproductId)
      .then(function (response) {

        invoiceItems.splice(index, 1)

        let total = me.state.invoiceItems.reduce((sum, item)=>{
          return sum + (item.price * item.quantity)
        }, 0)

        me.setState({
          total: me.priceWithDiscount(total, me.state.discount),
          productsOptions: me.toggleOptionSelectProduct(product_id),
          invoiceItems: invoiceItems
        })

      })
      .catch(function (error) {
        console.log(error);
      });

  }

  toggleOptionSelectProduct(product_id){
    let productsOptions = this.state.productsOptions.map((item)=>{
      if(item.value == product_id){
        return {value: item.value, label: item.label, price: item.price, disabled: !item.disabled}
      }
      return item
    })
    return productsOptions
  }

  priceWithDiscount(val, discount){
    return (val - (val / 100) * discount).toFixed(2)
  }

  onSave(){
    console.log("onSave: ")

    let invoice = {
      id: this.props.params.invoiceId,
      customer_id: this.state.customersValue.value,
      discount: this.state.discount,
      total:  this.state.total
    }

    this.props.commonActions.saveItem('invoices', invoice)

    this.state.invoiceItems.forEach((item)=>{
      axios.put('/api/invoices/'+this.state.id+'/items/'+item.id, item)
    })
  }

  handleOnChange = (e) => {
    if(e.target.id == 'discount'){
      let val = e.target.value.replace(/[^0-9]/g, "")
      if(val.charAt(0) == 0){
        val = val.slice(1)
      }
      if(val >= 0 && val <=100){
        this.setState({[e.target.id]: val})
      }
      if(val == ''){
        this.setState({[e.target.id]: 0})
      }

      let total = this.state.invoiceItems.reduce((sum, item)=>{
        return sum + (item.price * item.quantity)
      }, 0)
      this.setState({total:  this.priceWithDiscount(total, val)})

    }else{
      this.setState({[e.target.id]: e.target.value})
    }

  }

  logChange(action, val) {
    this.setState({[action]: val})
  }

  onChangeQty = (e) => {
    let index = e.target.id
    let val = e.target.value.replace(/[^0-9]/g, "")
    let invoiceItems = this.state.invoiceItems
    let item = invoiceItems[index]

    if(val == 0 || val == ''){
      val = 1
    }

    item.quantity = val
    invoiceItems[index] = item

    let total = this.state.invoiceItems.reduce((sum, item)=>{
      return sum + (item.price * item.quantity)
    }, 0)

    this.setState({
      invoiceItems: invoiceItems,
      total: this.priceWithDiscount(total, this.state.discount)
    })
  }

  generateTableBody(){
    return this.state.invoiceItems.map((item, index, invoiceItems)=>{
      return(
        <tr key={index}>
          <td>{item.label}</td>
          <td>{item.price}</td>
          <td><input value={invoiceItems[index].quantity} id={index} onChange={this.onChangeQty}/></td>
          <td><b><a href="" onClick={this.onDelete.bind(this, item.product_id, index)} style={{color: 'red'}}> x </a></b></td>
        </tr>
      )
    })
  }

  render() {

    const tableBody = this.generateTableBody()

    return (
      <div>
        <Form>
          <FormGroup controlId="discount">
            <ControlLabel>Discount (%)</ControlLabel>
            <FormControl
              type="text"
              value={this.state.discount}
              placeholder="Discount % (0-100)"
              onChange={this.handleOnChange}
            />
          </FormGroup>

          <FormGroup controlId="customer">
            <ControlLabel>Customer</ControlLabel>
            <Select
              name="form-field-customer"
              value={this.state.customersValue}
              options={this.state.customersOptions}
              onChange={this.logChange.bind(this, 'customersValue')}
            />
          </FormGroup>

          <FormGroup controlId="product">
            <ControlLabel>Add product</ControlLabel>
            <Select
              name="form-field-product"
              value={this.state.productsValue}
              options={this.state.productsOptions}
              onChange={this.logChange.bind(this, 'productsValue')}
            />
          </FormGroup>

          <Button onClick={::this.onAdd} >Add</Button>

        </Form>

        <Table responsive>
          <thead>
          <tr>
            <th>Name</th>
            <th>Price</th>
            <th>Qty</th>
            <th></th>
          </tr>
          </thead>
          <tbody>
            {tableBody}
          </tbody>
        </Table>

        <h1><b>Total: {this.state.total}</b></h1>

      </div>
    )
  }
}