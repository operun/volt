import React, {PropTypes} from 'react'
import {
  Button,
  Modal,
  Form,
  FormGroup,
  Col,
  ControlLabel,
  FormControl
} from 'react-bootstrap'

export default class ItemEdit extends React.Component {
  constructor(props) {
    super(props)
    let currentItem = {}
    this.props[this.props.type].Model.map((item)=>{
      currentItem[item.key] = this.props.common.currentItem[item.key] || ''
    })
    this.state = currentItem

  }

  generateForm(Model, isDeleting){
    return Model.map((item, index)=>{
      if(index == 0){//пропуcкаем id
        return
      }

      return(
        <FormGroup controlId={item.key} key={index}>
          <Col componentClass={ControlLabel} sm={2}>
            {item.label}
          </Col>
          <Col sm={10}>
            <FormControl type="text" placeholder={item.label} value={this.state[item.key]} onChange={this.handleOnChange} disabled={isDeleting} />
          </Col>
        </FormGroup>
      )
    })
  }

  handleOnChange = (e) => {
    this.setState({[e.target.id]: e.target.value}, null);
  }

  render() {

    const {type} = this.props
    const {showItem, currentItem, isSaving, isDeleting} = this.props.common
    const Model = this.props[type].Model
    const {closeItem, saveItem, deleteConfirmItem} = this.props.commonActions
    const formBody = this.generateForm(Model, isDeleting)

    const editTitle = (
      <Modal.Title>Edit {type} id: {currentItem.id} </Modal.Title>
    )
    const createTitle = (
      <Modal.Title>Create item </Modal.Title>
    )
    const deleteTitle = (
      <Modal.Title style={{color: "red"}}>Delete {type} id: {currentItem.id} </Modal.Title>
    )

    let modalTitle

    if(isDeleting){
      modalTitle = deleteTitle
    }else if(currentItem.id){
      modalTitle = editTitle
    }else{
      modalTitle = createTitle
    }

    return (
      <Modal show={showItem}>
        <Modal.Header >
          {
            modalTitle
          }
        </Modal.Header>
        <Modal.Body>
          <Form horizontal ref="form" >
            {formBody}
          </Form>
        </Modal.Body>
        <Modal.Footer>
          {
            isSaving ?
              <span>saving...</span> :
              null
          }
          {
            isDeleting ?
              <Button bsStyle="danger" onClick={() => deleteConfirmItem(type, this.state)}>Delete</Button> :
              <Button bsStyle="primary" onClick={() => saveItem(type, this.state)} disabled={isSaving}>Save</Button>
          }
          <Button onClick={closeItem}>Close</Button>
        </Modal.Footer>
      </Modal>
    )
  }
}

ItemEdit.propTypes = {
  type: React.PropTypes.string.isRequired,
  common: React.PropTypes.shape({
    showItem: React.PropTypes.bool.isRequired,
    currentItem: React.PropTypes.object.isRequired,
    isSaving: React.PropTypes.bool.isRequired,
    isDeleting: React.PropTypes.bool.isRequired
  }),
  commonActions: React.PropTypes.shape({
    closeItem: React.PropTypes.func.isRequired,
    saveItem: React.PropTypes.func.isRequired,
    deleteConfirmItem: React.PropTypes.func.isRequired
  })
}