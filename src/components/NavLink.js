import React from 'react'
import {Navbar, Nav, NavItem} from 'react-bootstrap'
import {IndexLinkContainer, LinkContainer} from 'react-router-bootstrap'

export default class NavLink extends React.Component {
  render() {
    return (
      <Navbar>
        <Navbar.Header>
          <Navbar.Brand>
            <a href="/">Invoice App</a>
          </Navbar.Brand>
        </Navbar.Header>
        <Nav>
          <IndexLinkContainer to="/"><NavItem eventKey={1}>Invoices</NavItem></IndexLinkContainer>
          <LinkContainer to="/products"><NavItem eventKey={2}>Products</NavItem></LinkContainer>
          <LinkContainer to="/customers"><NavItem eventKey={3}>Customers</NavItem></LinkContainer>
        </Nav>
      </Navbar>
    )
  }
}
