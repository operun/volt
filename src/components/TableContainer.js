import React, {Component, PropTypes} from 'react'
import {Table} from 'react-bootstrap'
import { Link } from 'react-router'

export default class TableContainer extends Component {
  onBtnClick(action, id, e) {
    e.preventDefault()
    let currentItem = this.props[this.props.type].allItems.filter((item)=> {
      return id == item.id
    })
    this.props.commonActions[action](currentItem[0])
  }

  generateCols(cols) {
    return cols.map((item)=>{
      return <th key={item.key}>{item.label}</th>
    })
  }

  generateRows(cols, data, type) {
    return data.map((item, dataIndex)=>{
      let cells = cols.map((colData, index)=>{
        if(index == 0){
          // return <td key={index}>{dataIndex+1}</td> //раскомментировать (отображение порядковых номеров, вместо их id)
        }

        //вписываем имя покупателя вместо его id (только на странице Invoices)
        if(type == 'invoices'){
          if(colData.key == 'customer_id'){
            let customerName = <td key={index}></td>
            this.props.customers.allItems.forEach((customerItem)=>{
              if(customerItem.id == item[colData.key]){
                customerName = <td key={index}>{customerItem.name}</td>
              }
            })
            return customerName
          }
        }

        return <td key={index}>{item[colData.key]}</td>
      })

      return(
        <tr key={item.id}>
          {cells}
          
          {
            type == 'invoices' ?
              <th><Link to={'/invoices/'+item.id+'/edit'} >edit</Link></th>
              : <th><a href="" onClick={this.onBtnClick.bind(this, 'editItem', item.id)}>edit</a></th>
          }

          <th><b><a href="" onClick={this.onBtnClick.bind(this, 'deleteItem', item.id)} style={{color: 'red'}}> x </a></b></th>
        </tr>
      )
    })
  }

  render() {

    const {type} = this.props
    const Model = this.props[type].Model
    const allItems = this.props[type].allItems

    const tableCols = this.generateCols(Model)
    const tableRows = this.generateRows(Model, allItems, type)

    return (
      <Table responsive>
        <thead>
        <tr>
          {tableCols}
        </tr>
        </thead>
        <tbody>
          {tableRows}
        </tbody>
      </Table>
    )
  }
}

TableContainer.PropTypes = {
  type: React.PropTypes.string.isRequired
}