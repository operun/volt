import React, {Component} from 'react'
import NavLink from '../components/NavLink'

export default class AppContainer extends Component {
  render() {
    return (
      <div>
        <NavLink/>
        <div className="container">
          {this.props.children}
        </div>
      </div>
    )
  }
}
