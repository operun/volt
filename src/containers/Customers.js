import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import * as commonActions from '../actions/CommonActions'
import TableContainer from '../components/TableContainer'
import {PageHeader} from 'react-bootstrap'
import ItemEdit from '../components/ItemEdit'

class Customers extends Component {
  constructor(props){
    super(props)
    this.state = {
      type: 'customers'
    }
  }
  componentDidMount() {
    document.title = "Customers"
    if(!this.props[this.state.type].isLoaded){
      this.props.commonActions.getItems(this.state.type)
    }
  }

  render() {

    const {fetching, showItem} = this.props.common
    const {editItem} = this.props.commonActions

    return (
      <div>
        <PageHeader>Customers list &nbsp;
          <button className="btn btn-default" onClick={editItem}>Create</button>
        </PageHeader>

        {
          fetching ?
            <p>Loading...</p> :
            <TableContainer {...this.props} type={this.state.type} />
        }

        {
          showItem ?
            <ItemEdit {...this.props} type={this.state.type}/> :
            null
        }

      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    common: state.common,
    customers: state.customers
  }
}

function mapDispatchToProps(dispatch) {
  return {
    commonActions: bindActionCreators(commonActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Customers)