import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import * as commonActions from '../actions/CommonActions'
import InvoiceEdit from '../components/InvoiceEdit'

class Invoice extends React.Component {

  componentDidMount() {
    if(!this.props.products.isLoaded){
      this.props.commonActions.getItems('products')
    }
    if(!this.props.customers.isLoaded){
      this.props.commonActions.getItems('customers')
    }
  }

  static urlCheck(nextState, replace) {
    let invoiceId = nextState.params.invoiceId
    // if (invoiceId == 'new') {
    //   return
    // }
    if(invoiceId == parseInt(invoiceId, 10)){
      return
    }
    replace('/invoice-not-found')
  }

  render() {

    return (
      <div>
        {
          this.props.params.invoiceId == 'new' ?
            <h1>Create Invoice</h1>
            : <h1>Edit Invoice id: {this.props.params.invoiceId}</h1>
        }

        {
          this.props.products.isLoaded &&
          this.props.customers.isLoaded ?
          <InvoiceEdit {...this.props} />
          : <p>Loading...</p>
        }

      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    common: state.common,
    products: state.products,
    customers: state.customers
  }
}

function mapDispatchToProps(dispatch) {
  return {
    commonActions: bindActionCreators(commonActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Invoice)