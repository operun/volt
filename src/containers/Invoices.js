import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import * as commonActions from '../actions/CommonActions'
import * as invoiceActions from '../actions/InvoiceActions'
import TableContainer from '../components/TableContainer'
import {PageHeader} from 'react-bootstrap'
import { Link } from 'react-router'
import axios from 'axios'
import ItemEdit from '../components/ItemEdit'

class Invoices extends Component {
  constructor(props){
    super(props)
    this.state = {
      type: 'invoices'
    }
  }
  componentDidMount() {
    document.title = "Invoices"

    if(!this.props[this.state.type].isLoaded){
      this.props.commonActions.getItems(this.state.type)
    }
    if(!this.props.customers.isLoaded){
      this.props.commonActions.getItems('customers')
    }
  }

  onCreate(){
    this.props.invoiceActions.createInvoice()
  }

  render() {

    const {fetching, showItem} = this.props.common

    let table = null;
    if (fetching || !this.props.customers.isLoaded) {
      table = <p>Loading...</p>
    } else {
      table = <TableContainer {...this.props} type={this.state.type} />
    }

    return (
      <div>
        <PageHeader>Invoices list &nbsp;
          {/*<Link to={'/invoices/new'}><button className="btn btn-default">Create</button></Link>*/}
          <button className="btn btn-default" onClick={::this.onCreate}>Create</button>
        </PageHeader>

        {table}

        {
          showItem ?
            <ItemEdit {...this.props} type={this.state.type}/> :
            null
        }

      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    common: state.common,
    customers: state.customers,
    invoices: state.invoices
  }
}

function mapDispatchToProps(dispatch) {
  return {
    commonActions: bindActionCreators(commonActions, dispatch),
    invoiceActions: bindActionCreators(invoiceActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Invoices)