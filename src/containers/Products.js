import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import * as commonActions from '../actions/CommonActions'
import TableContainer from '../components/TableContainer'
import {PageHeader} from 'react-bootstrap'
import ItemEdit from '../components/ItemEdit'

class Products extends Component {
  constructor(props){
    super(props)
    this.state = {
      type: 'products'
    }
  }
  componentDidMount() {
    document.title = "Products"

    if(!this.props[this.state.type].isLoaded){
      this.props.commonActions.getItems(this.state.type)
    }
  }

  render() {

    const {fetching, showItem} = this.props.common
    const {editItem} = this.props.commonActions

    return (
      <div>
        <PageHeader>Products list &nbsp;
          <button className="btn btn-default" onClick={editItem}>Create</button>
        </PageHeader>

        {
          fetching ?
            <p>Loading...</p> :
            <TableContainer {...this.props} type={this.state.type} />
        }

        {
          showItem ?
            <ItemEdit {...this.props} type={this.state.type}/> :
            null
        }

      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    common: state.common,
    products: state.products
  }
}

function mapDispatchToProps(dispatch) {
  return {
    commonActions: bindActionCreators(commonActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Products)