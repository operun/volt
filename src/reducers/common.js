const initialState = {
  fetching: false,
  currentItem: {},
  showItem: false,
  isSaving: false,
  isDeleting: false
  // itemModel:{
  //   invoiceItems: [
  //     {key: 'id', label: '#'},
  //     {key: 'invoice_id', label: 'Invoice_id'},
  //     {key: 'product_id', label: 'Product_id'},
  //     {key: 'quantity', label: 'Quantity'}
  //   ]
  // }
}

export default function items(state = initialState, action) {

  switch (action.type) {

    case 'SET_CURRENT_ITEM':
      return {...state, currentItem: action.payload}

    case 'EDIT_ITEM':
      return {...state, currentItem: action.payload, showItem: true}

    case 'CLOSE_ITEM':
      return {...state, currentItem: {}, showItem: false, isDeleting: false}

    case 'SAVE_ITEM_REQUEST':
      return {...state, isSaving: true}

    case 'SAVE_ITEM_SUCCESS':
      return {...state, currentItem: {}, showItem: false, isSaving: false}

    case 'UPDATE_ITEM_SUCCESS':
      return {...state, currentItem: {}, showItem: false, isSaving: false}

    case 'DELETE_ITEM':
      return {...state, currentItem: action.payload, showItem: true, isDeleting: true}

    case 'DELETE_ITEM_SUCCESS':
      return {...state, currentItem: {}, showItem: false, isDeleting: false}

    default:
      return state;
  }
}