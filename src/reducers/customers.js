import {
  GET_REQUEST_CUSTOMERS,
  GET_SUCCESS_CUSTOMERS,
  SAVE_SUCCESS_CUSTOMERS,
  UPDATE_SUCCESS_CUSTOMERS,
  DELETE_SUCCESS_CUSTOMERS
} from '../constants/CustomersConstants'

const initialState = {
  allItems: [],
  fetching: false,
  isLoaded: false,
  Model: [
    {key: 'id', label: '#'},
    {key: 'name', label: 'Name'},
    {key: 'address', label: 'Address'},
    {key: 'phone', label: 'Phone'}
  ],
}

export default function items(state = initialState, action) {

  switch (action.type) {
    case GET_REQUEST_CUSTOMERS:
      return {...state, fetching: true}

    case GET_SUCCESS_CUSTOMERS:
      return {...state, allItems: action.payload, fetching: false, isLoaded: true}

    case SAVE_SUCCESS_CUSTOMERS:
      // return {...state, allItems: state.allItems.concat(action.payload.currentItem)}
      return {...state, allItems: [...state.allItems, action.payload.currentItem] }

    case UPDATE_SUCCESS_CUSTOMERS:
      return {
        ...state,
        allItems: state.allItems.map((item)=> {
          if (action.payload.id == item.id) {
            return action.payload
          } else {
            return item
          }
        })
      }

    case DELETE_SUCCESS_CUSTOMERS:
      let newArr = []
      state.allItems.forEach(function(item) {
        if (action.payload.id != item.id) {
          newArr.push(item)
        }
      });

      return {...state, allItems: newArr}

    default:
      return state;
  }
}