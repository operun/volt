import {combineReducers} from 'redux'
import common from './common'
import customers from './customers'
import products from './products'
import invoices from './invoices'

export default combineReducers({
  common,
  customers,
  products,
  invoices
})
