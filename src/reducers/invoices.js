import {
  GET_REQUEST_INVOICES,
  GET_SUCCESS_INVOICES,
  SAVE_SUCCESS_INVOICES,
  UPDATE_SUCCESS_INVOICES,
  DELETE_SUCCESS_INVOICES
} from '../constants/InvoicesConstants'

const initialState = {
  allItems: [],
  fetching: false,
  isLoaded: false,
  Model: [
    {key: 'id', label: '#'},
    {key: 'customer_id', label: 'Customer'},
    {key: 'discount', label: 'Discount'},
    {key: 'total', label: 'Total'}
  ]
}

export default function items(state = initialState, action) {

  switch (action.type) {
    case GET_REQUEST_INVOICES:
      return {...state, fetching: true}

    case GET_SUCCESS_INVOICES:
      return {...state, allItems: action.payload, fetching: false, isLoaded: true}

    case SAVE_SUCCESS_INVOICES:
      // return {...state, allItems: state.allItems.concat(action.payload.currentItem)}
      return {...state, allItems: [...state.allItems, action.payload.currentItem] }

    case UPDATE_SUCCESS_INVOICES:
      return {
        ...state,
        allItems: state.allItems.map((item)=> {
          if (action.payload.id == item.id) {
            return action.payload
          } else {
            return item
          }
        })
      }

    case DELETE_SUCCESS_INVOICES:
      let newArr = []
      state.allItems.forEach(function(item) {
        if (action.payload.id != item.id) {
          newArr.push(item)
        }
      });

      return {...state, allItems: newArr}

    default:
      return state;
  }
}