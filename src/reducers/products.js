import {
  GET_REQUEST_PRODUCTS,
  GET_SUCCESS_PRODUCTS,
  SAVE_SUCCESS_PRODUCTS,
  UPDATE_SUCCESS_PRODUCTS,
  DELETE_SUCCESS_PRODUCTS
} from '../constants/ProductsConstants'

const initialState = {
  allItems: [],
  fetching: false,
  isLoaded: false,
  Model: [
    {key: 'id', label: '#'},
    {key: 'name', label: 'Name'},
    {key: 'price', label: 'Price'}
  ]
}

export default function items(state = initialState, action) {

  switch (action.type) {
    case GET_REQUEST_PRODUCTS:
      return {...state, fetching: true}

    case GET_SUCCESS_PRODUCTS:
      return {...state, allItems: action.payload, fetching: false, isLoaded: true}

    case SAVE_SUCCESS_PRODUCTS:
      // return {...state, allItems: state.allItems.concat(action.payload.currentItem)}
      return {...state, allItems: [...state.allItems, action.payload.currentItem] }

    case UPDATE_SUCCESS_PRODUCTS:
      return {
        ...state,
        allItems: state.allItems.map((item)=> {
          if (action.payload.id == item.id) {
            return action.payload
          } else {
            return item
          }
        })
      }

    case DELETE_SUCCESS_PRODUCTS:
      let newArr = []
      state.allItems.forEach(function(item) {
        if (action.payload.id != item.id) {
          newArr.push(item)
        }
      });

      return {...state, allItems: newArr}

    default:
      return state;
  }
}