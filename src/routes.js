import React from 'react'
import {Route, IndexRoute} from 'react-router'
import AppContainer from './containers/AppContainer'
import Customers from './containers/Customers'
import Products from './containers/Products'
import Invoices from './containers/Invoices'
import InvoiceEdit from './containers/Invoice'
import NotFound from './containers/NotFound'

export const routes = (
  <div>
    <Route path='/' component={AppContainer}>
      <IndexRoute component={Invoices}/>
      <Route path='/invoices/:invoiceId/edit' component={InvoiceEdit} onEnter={InvoiceEdit.urlCheck}/>
      {/*<Route path='/invoices/:invoiceId' component={InvoiceEdit} onEnter={InvoiceEdit.urlCheck}/>*/}
      <Route path='/products' component={Products}/>
      <Route path='/customers' component={Customers}/>
    </Route>
    <Route path='*' component={NotFound}/>
  </div>
)
